function getPrices() {
    return {
        prodTypes: [80, 330, 300],
        prodOptions: {
            option1: 1,
            option2: 0.5,
        },
        prodSv: {
            option3: 20,
            option4: 0,
        },
    };
}




function updatePrice() {
    let k = document.getElementById("kol").value;
    if (k.match(/^[0-9]+$/) === null) {
        document.getElementById("prodPrice").innerHTML = "Неверно";
        return false;
    } else {
        return true;
    }
}


function price() {
    let vid = document.getElementById("prodType");
    let kol = document.getElementById("kol").value;
    let rad = document.getElementById("radios");
    let boxes = document.getElementById("checkboxes");
    let price = document.getElementById("prodPrice");
    prodPrice.innerHTML = "0";
    switch (vid.value) {
        case "1":
            rad.style.display = "none";
            boxes.style.display = "none";
            if (updatePrice()) {
                let itog = parseInt(kol) * getPrices().prodTypes[0];
                prodPrice.innerHTML = itog + " рублей";
            }
            break;
        case "2":
            rad.style.display = "block";
            boxes.style.display = "none";
            let c;
            let rb = document.getElementsByName("prodOptions");
            for (var i = 0; i < rb.length; i++) {
                if (rb[i].checked) {
                    c = rb[i].value;
                }
            }
            let l = getPrices().prodOptions[c];
            if (updatePrice()) {
                let itog = getPrices().prodTypes[1] * l * parseInt(kol);
                prodPrice.innerHTML = itog + " рублей";
            }
            break;
        case "3":
            rad.style.display = "none";
            boxes.style.display = "block";
            let sum = 0;
            let d = document.getElementsByName("prodSv");
            for (let i = 0; i < d.length; i++) {
                if (d[i].checked) {
                    sum += getPrices().prodSv[d[i].value];
                }
            }
            if (updatePrice()) {
                let itog = parseInt(kol) * (getPrices().prodTypes[2] + sum);
                price.innerHTML = itog + " рублей";
            }
            break;
    }
}



window.addEventListener("DOMContentLoaded", function(event) {
    let r = document.getElementById("radios");
    r.style.display = "none";
    let b = document.getElementById("checkboxes");
    b.style.display = "none";

    let kol = document.getElementById("kol");
    kol.addEventListener("change", function(event) {
        console.log("Was changed");
        price();
    });

    let select = document.getElementById("prodType");
    select.addEventListener("change", function(event) {
        console.log("Was changed");
        price();
    });

    let radio = document.getElementsByName("prodOptions");
    radio.forEach(function(radio) {
        radio.addEventListener("change", function(event) {
            console.log("Was changed");
            price();
        });
    });
    let checkbox = document.querySelectorAll("#checkboxes input");
    checkbox.forEach(function(checkbox) {
        checkbox.addEventListener("change", function(event) {
            console.log("Was changed");
            price();
        });
    });
    price();
});